#!/usr/bin/env python
# coding: utf-8
import numpy as np 
import matplotlib.pylab as plt
#import USED_FUNCTIONS as UF
import sys
# insert at 1, 0 is the script path (or '' in REPL)
# sys.path.insert(1, '/bettik/boudaose/FINAL_SCRIPTS/') 
sys.path.insert(1, '/bettik/boudaose/DATASET_CHAMBERY/')
import USED_FUNCTIONS_2 as UF

########################################## Parameters ################################################
#Ran, Azi           = 14204, 6604   # Top-Left of the General map. (Annecy)
#ShapeRan, ShapeAzi = 1000, 4000    # (Annecy)

Ran, Azi = 16004, 9004     # Top-Left of the General map. (Chambery)
ShapeRan, ShapeAzi = 1600, 6400

#################################
#Put a color 1 -> 41 
COLORR = 31
#LColors = [ 1, 2, 12, 18, 23, 24, 26, 40, 41 ] # CLC labels
################################# 
DATA_PER_LABEL = 10000   # as a max  
PASC    = 12*1        # Distance between patches (Range axis ) 
PASL    = 3*1         # Distance between patches (Azimut axis) 
NbDates = 60          # 60 = 1 year
################################
print( "Color :", COLORR, "Data/Lab :", DATA_PER_LABEL,) 
print( "STEPS :", PASC, PASL, "Through:", NbDates, "dates" ) 
# The CLC CROP of the studied region
################################
#filename = f"/bettik/boudaose/Annecy/CLC_Ran-Azi_{Ran}_{Azi}_Shapes_{ShapeRan}_{ShapeAzi}.dat" # Annecy

filename = f"/home/boudaose/23_mai/Chambery/CLC_Ran-Azi_{Ran}_{Azi}_Shapes_{ShapeRan}_{ShapeAzi}.dat"         # Chambery

##########################################             ################################################
# PATHS
#post = "local"
post = "cluster"
if ( post == 'local' ) :
    path_r     = f"/home/zwartholm/Desktop/Stages/STAGE/Data/" 
    path_slc   = path_r # (here the same) 
    path_dates = f"/home/zwartholm/Desktop/Stages/STAGE/Data/"
    path_geom  = f"/home/zwartholm/Desktop/Stages/STAGE/Data/"
elif ( post == 'cluster' ) : 
    path_r     = f"/summer/deformalpes/ARCHIVE/pauline/SAUV_D139_2021/iw2/iw2/" 
    path_slc   = path_r # (here the same) 
    path_dates = f"/summer/deformalpes/ARCHIVE/pauline/SAUV_D139_2021/iw2/iw2/"
    path_geom  = f"/bettik/doinm/ALPS/D139/TMP/"
############################################################
# We load the Radar-Geometry CLC crop computed in the Task_1
with open( filename, 'rb') as f:
    a = np.load(f) 
CLC = a 
size = 15
from matplotlib.pyplot import figure ; 
figsize = size
figure(facecolor=(1,1,1), num=None,
       figsize=(figsize, figsize ), 
       dpi=80, )
plt.imshow( a, cmap='jet_r', vmin=0, vmax=45 ) 
plt.colorbar(  fraction=0.0200, pad=0.11 ) 
#plt.savefig( "CLCcalculated.png" )
#plt.show() 
plt.close()
############################################################
############################################################
a = CLC
PresentColors = list(set(a.flatten(order='C'))) 
print( f"Il y a {len(PresentColors)} differentes Couleurs " )

for i in range(len(PresentColors)) : 
    print(  int(PresentColors[i]), '\t->' , UF.LabelsList()[int(PresentColors[i])] )

####
color = COLORR
s     = 3 
#SHFT  = 8
SHFT = 1 # To go in deep into a color region and ignore the first layers of pxl
Kernel=[s*4+SHFT*4,s*1+SHFT] 
# Ajouter input : Start Point & Step of Search
List_Toplefts = UF.FindGoodAreas( CLC[:,:],
                              color  = color  ,
                              Kernel = Kernel ,
                              startpointC = 0 ,
                              startpointR = 0 ,
                              stepsearchC = 1 ,
                              stepsearchR = 1 ,
                              shiftUL_R   = int((SHFT*4)/2) ,
                              shiftUL_A   = int(SHFT/2)     ,
                              show=False  ) 
#----------------------------------------------
####

# Save the TopLeft list
filename = str(f"Toplefts_Label{color}_Kernel{Kernel}.list").replace(' ','_') 
with open( filename, 'wb') as f:
    np.save(f, List_Toplefts ) 
#____________________________________________________________________________
#____________________________________________________________________________
#____________________________________________________________________________

# Load the TopLeft list
filename = str(f"Toplefts_Label{color}_Kernel{Kernel}.list").replace(' ','_') 
with open( filename, 'rb') as f:
    a = np.load(f) 

print(UF.LabelsList()[color]) 
print(color) 

C0, L0 = Ran, Azi
print(C0, L0) 
List_Toplefts = a
print(len( List_Toplefts )) 

##############################################         ###############################################
# From the available top-lefts list, we will select the coordinates so that the patches do not overlap.
X, Y = [] , []
for i in range(0, len( List_Toplefts ) ) : 
    X.append(List_Toplefts[i][1]+ Ran%100 )               # + Ran )     # range   
    Y.append(List_Toplefts[i][0]+ Azi%100 )               # + Azi )     # azimuth 

print( "Length of X :" , len(X)) 
print('') 

separateList = [] 
Xsep, Ysep   = [], [] 
GoodList     = [] 
gX           = [] 
gY           = [] 
#----------------------------------------------
gX.append( 0  ) # # range   
gY.append( 0  ) # # azimuth 

#----------------------------------------------
for i in range( 0, len( List_Toplefts ), 1 ) : 
    # Number of patches as a maximum
    if ( len(gX)>int(DATA_PER_LABEL) ) : 
        break
    ifs = 0 
    for j in range( 0, len( gX ) ) : 
        cond1 = ( abs( gX[j]-X[i] ) >= PASC  )         # PASC pxl by step
        cond2 = ( abs( gY[j]-Y[i] ) >= PASL  )         # PASL pxl by step
        if (cond1+cond2) :
            ifs += 2
    if ( ifs == 2*len(gX) ) : 
        gX.append( X[i] ) # # range 
        gY.append( Y[i] ) # # azimuth 
#----------------------------------------------
print( "Length of gX :" , len(gX) ) 
print( "Length of gY :" , len(gY) ) 


X, Y = gX[1:], gY[1:] 
print(Ran, Azi, Ran%100, Azi%100) 
print([CLC.shape[1]+0 , CLC.shape[0]+0] ) 

coords   = [] # 
C0ppL0pp = [] # 
i = 0 
###############################################

##########################|##############################
coords=[] 
for i in range ( len(X) ) : 
    coords.append( (X[i]+(Ran-Ran%100), 
                    Y[i]+(Azi-Azi%100) ) ) 

##############################|##############################
import os
directory = f"Label_{color}"
if not os.path.exists(directory):
    os.makedirs(directory) 

import os
directory = f"Label_{color}/Context_Label_{color}"
if not os.path.exists(directory):
    os.makedirs(directory) 

import os
directory = f"Label_{color}/Imagette_Label_{color}"
if not os.path.exists(directory):
    os.makedirs(directory)

######################################################
filename = f"Label_{color}/Coordinates_XY_{color}" 
with open( filename, 'wb') as f : 
    np.save(f, coords ) 

# Load the TopLeft list
filename = f"Label_{color}/Coordinates_XY_{color}" 
with open( filename, 'rb') as f:
    coords = np.load(f) 

LxLy = list(zip( *coords )) 
X, Y = LxLy[0], LxLy[1] 
print(Ran, Azi)

import os
directory = f"Label_{color}/Context_500x500_{color}" 
if not os.path.exists(directory):
    os.makedirs(directory) 

######################################################
Ndata = len(coords) 
print( "Number of actual available Data ", Ndata )

# Load the coordinates 
filename = f"Label_{color}/Coordinates_XY_{color}" 
with open( filename, 'rb') as f:
    coords = np.load(f) 

############################################################
LxLy = list(zip( *coords )) 
X, Y = LxLy[0], LxLy[1] 
L = UF.TimeInterval( start_date = "20170309", 
                    Nb_Dates = NbDates, 
                nb_dates_max = 215  ) 
############################################################

import os
directory = f"Label_{color}"
if not os.path.exists(directory):
    os.makedirs(directory) 
import os
directory = f"Label_{color}/Context_Label_{color}"
if not os.path.exists(directory):
    os.makedirs(directory) 
import os
directory = f"Label_{color}/Imagette_Label_{color}"
if not os.path.exists(directory):
    os.makedirs(directory)

# Storing Imagettes (Patches)
for i in range( 0, Ndata, 1 ) : 
    C0, L0 = X[i], Y[i] 
    print( "Imagettes :", i, "c0, l0 :", X[i]%100 , Y[i]%100 , 
            "Absolute \t |-->", "C0, L0 :", 
             X[i] , Y[i]  ) 
    
    for j in range( 0, len(L)  ) : 
        date1 = str(L[j]) 
        _, S1, _ = UF.ExtractCorrectContextCrop( C0, L0,
                                           date1   = date1,
                                           Imgt    = [12,3],
                                           CONTEXT = [120,120] ) 
        
        filename = f'Label_{color}/'+f'Imagette_Label_{color}/Pxl_{i}_C0{C0}_L0{L0}_{date1}.dat'
        with open( filename, 'wb') as f:
            np.save(f, S1 ) 

###############################################################
# Computing the Coherence Matrices
import os
directory = f"Label_{color}/Coherence_{color}"
if not os.path.exists(directory): 
    os.makedirs(directory) 

## Computing Coherence Matrix + Stack
for i in range( 0, Ndata, 1  ) : 
    C0, L0 = X[i], Y[i] 
    Pxl    = i 
    print( "Coherence : ", i, C0, L0 ) 

    try    :
        #Coh, Cov, CovA, Corr = COH_COV_COVA( ListOfDates=L, C0=C0, L0=L0, Color=color, Pxl=Pxl ) 
        Coh, Cov, CovA = UF.COH_COV_COVA( ListOfDates=L, C0=C0, L0=L0, Color=color, Pxl=Pxl, Channels=3 ) 
    except :
        Coh  = np.zeros( [len(L),len(L)] )*np.nan
        Cov  = np.zeros( [len(L),len(L)] )*np.nan
        CovA = np.zeros( [len(L),len(L)] )*np.nan
        #Corr = np.zeros( [len(L),len(L)] )*np.nan
        pass
    title  = str(f'Color {color}, Pixel {Pxl}, On {len(L)} dates').replace(' ','_') 
    filename1 = f"Label_{color}/Coherence_{color}/"+f'Coherence_Matrix_{title}.mat' 
    with open( filename1, 'wb') as f : 
        np.save(f, Coh ) 
    filename2 = f"Label_{color}/Coherence_{color}/"+f'Covariance_Matrix_{title}.mat' 
    with open( filename2, 'wb') as f : 
        np.save(f, Cov ) 
    filename3 = f"Label_{color}/Coherence_{color}/"+f'CovarianceA_Matrix_{title}.mat' 
    with open( filename3, 'wb') as f : 
        np.save(f, CovA ) 

    #filename4 = f"Label_{color}/Coherence_{color}/"+f'Correlation_Matrix_{title}.mat' 
    #with open( filename4, 'wb') as f : 
    #    np.save(f, Corr ) 

# Computing 2 other channels (Patches of Pure-Avg, from the Stack)
for i in range( 0, Ndata, 1  ) : 
    C0, L0 = X[i], Y[i] 
    Pxl    = i 
    print( "Stack_Amp-Phz : ", i, C0, L0 ) 
    title  = str(f'Color {color}, Pixel {Pxl}, On {len(L)} dates').replace(' ','_')
    AMPSTAC, PHZSTAC, _        = UF.Complex_Imagt_BIL( C0, L0, Date='30211332', ImgtBIL=[12,3]  , chemin='stack' )
    filename5 = f"Label_{color}/Coherence_{color}/"+f'StackAmp_{title}.mat' 
    with open( filename5, 'wb') as f : 
        np.save(f, AMPSTAC ) 
    filename6 = f"Label_{color}/Coherence_{color}/"+f'StackPhz_{title}.mat' 
    with open( filename6, 'wb') as f : 
        np.save(f, PHZSTAC ) 



###############################################################

print("couleur : ", color)

################################################################################
################################################################################
################################################################################

