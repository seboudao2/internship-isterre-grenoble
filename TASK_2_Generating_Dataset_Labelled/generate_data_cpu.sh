#!/bin/bash
# 1/ create an OUTPUT folder
## === 2. List of arguments ===
#OAR -l /nodes=1/core=4,walltime=5:00:00
#OAR -n TASK_2_tuto
#OAR --stdout /bettik/boudaose/FINAL_SCRIPTS/TASK2_Generating_Labeled_Dataset/OUTPUT/O_job.%jobid%.out
#OAR --stderr /bettik/boudaose/FINAL_SCRIPTS/TASK2_Generating_Labeled_Dataset/OUTPUT/O_job.%jobid%.err
#OAR --project ml-remotesens



## === 3. Purge and load needed modules ===
source /applis/environments/conda.sh
conda activate GPU

## === 5. Running the program ===

time python3 -u /bettik/boudaose/FINAL_SCRIPTS/TASK2_Generating_Labeled_Dataset/Computing_2_cohcovcovacorr.py

